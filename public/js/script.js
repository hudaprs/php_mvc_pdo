$(function(){


	$('.tombolTambahData').on('click', function(){
		$('#formModalLabel').html('Tambah Data Mahasiswa');
		$('.modal-footer button[type=submit]').html('Tambah Data');
		$('.modal-body form').trigger('reset');
	});

	$('.tampilModalUbah').on('click', function(){
		$('#formModalLabel').html('Ubah Data Mahasiswa');
		$('.modal-footer button[type=submit]').html('Ubah Data');
		$('.modal-body form').attr('action', 'http://localhost/HudaPrasetyo/phpclass2/mvc/public/mahasiswa/ubah');

		const id = $(this).data('id');

		$.ajax({
			url: 'http://localhost/HudaPrasetyo/phpclass2/mvc/public/mahasiswa/getubah',
			data: {id : id},
			method: 'post',
			dataType: 'json',
			success: function( data ){
				$('#nama').val(data.nama);
				$('#email').val(data.email);
				$('#jurusan').val(data.jurusan);
				$('#umur').val(data.umur);
				$('#id').val(data.id);
			}
		});
	});

});