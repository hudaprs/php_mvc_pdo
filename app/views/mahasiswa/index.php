<div class="container mt-3">

    <div class="row">
        <div class="col-lg-6">
            <?php Flasher::flash(); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-6">

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary tombolTambahData" data-toggle="modal" data-target="#formModal">
        Tambah Mahasiswa
        </button>

         <div class="row mt-3">
            <div class="col-6">
            <form  action="<?php echo BASEURL; ?>/mahasiswa/cari" method="post">
            <div class="input-group">
                <input type="text" name="keyword" class="form-control" placeholder="Cari data mahasiswa">
                <div class="input-group-append">
                <button class="btn btn-outline-primary" type="submit" id="button-addon2"> Cari </button>
                </div>  
            </div>
            </form>
            </div>
        </div>

        <?php if ( empty($data['mhs']) ){
            ?>
            <div class="row">
                <div class="col-6">
                    <div class="alert alert-danger"> Data mahasiswa tidak ditemukan </div>
                </div>
            </div>
            <?php
             }
            ?>

            
            <br>
            <br>

            <h3>Daftar Mahasiswa</h3>
                <ul class="list-group">
                <?php foreach ( $data['mhs'] as $mhs ) :  ?>
                <li class="list-group-item ">
                    <?php echo $mhs['nama']; ?>
                    <a href="<?php echo BASEURL; ?>/mahasiswa/hapus/<?php echo $mhs['id']; ?>" onclick="return confirm('yakin ingin menghapus?');" class="badge badge-danger float-right ml-1"> Delete </a>
                    <a href="<?php echo BASEURL; ?>/mahasiswa/ubah/<?php echo $mhs['id']; ?>" class="badge badge-success float-right ml-1 tampilModalUbah" data-toggle="modal" data-target="#formModal" data-id="<?php echo $mhs['id']; ?>"> Ubah </a>
                    <a href="<?php echo BASEURL; ?>/mahasiswa/detail/<?php echo $mhs['id']; ?>" class="badge badge-primary float-right ml-1"> Detail </a>
                </li>
                <?php endforeach; ?>
                </ul>
        </div>
    </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formModalLabel">Tambah Mahasiswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form action="<?php echo BASEURL; ?>/mahasiswa/tambah" method="post">
        <input type="hidden" name="id" id="id">
            <div class="form-group">
                <label for="nama">Nama Lengkap</label>
                <input type="text" name="nama" class="form-control" id="nama">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="example@example.com">
            </div>

             <div class="form-group">
                <label for="jurusan">Jurusan</label>
                <select class="form-control" name="jurusan" id="jurusan">
                    <option value="Informatika">Informatika</option>
                    <option value="Teknik Mesin">Teknik Mesin</option>
                    <option value="Kimia Analis">Kimia Analis</option>
                    <option value="Pharmacy">Pharmacy</option>
                    <option value="Jaringan">Jaringan</option>
                    <option value="Sosial">Sosial</option>
                </select>
            </div>

            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" name="umur" class="form-control" id="umur">
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
        </form>
      </div>
    </div>
  </div>
</div>