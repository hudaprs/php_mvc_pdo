<?php 

	class Mahasiswa_model{
		private $db;

		public function __construct(){
			$this->db = new Database;
		}

		public function getAllMahasiswa(){
			$this->db->query("SELECT * FROM mahasiswa");
			return $this->db->resultSet();
		}

		public function getMahasiswaById( $id ){
			$this->db->query("SELECT * FROM mahasiswa WHERE id=:id");
			$this->db->bind('id', $id);
			return $this->db->single();
		}

		public function tambahDataMahasiswa( $data ){
			$query = " INSERT INTO mahasiswa VALUES ('', :nama, :jurusan, :email, :umur)";

			$this->db->query( $query );

			$this->db->bind('nama', $data['nama']);
			$this->db->bind('jurusan', $data['jurusan']);
			$this->db->bind('email', $data['email']);
			$this->db->bind('umur', $data['umur']);

			$this->db->execute();

			return $this->db->rowCount();
		}

		public function hapusDataMahasiswa( $id ){

			$this->db->query("DELETE FROM mahasiswa WHERE id=:id");

			$this->db->bind('id', $id);

			$this->db->execute();

			return $this->db->rowCount();
		}

		public function ubahDataMahasiswa( $data ){
			$query = "UPDATE mahasiswa SET nama=:nama,
										   email=:email,
										   jurusan=:jurusan,
										   umur=:umur
										   WHERE id=:id";
			$this->db->query($query);
			$this->db->bind('id', $data['id']);							   
			$this->db->bind('nama', $data['nama']);							   
			$this->db->bind('email', $data['email']);							   
			$this->db->bind('jurusan', $data['jurusan']);							   
			$this->db->bind('umur', $data['umur']);		

			$this->db->execute();
			return $this->db->rowCount();					   
		}

		public function cariDataMahasiswa(){
			$keyword = $_POST['keyword'];
			$this->db->query("SELECT * FROM mahasiswa WHERE nama LIKE :keyword
														OR jurusan LIKE :keyword
														OR email LIKE :keyword");
			$this->db->bind('keyword', "%$keyword%");

			return $this->db->resultSet();
		}

		
	}

 ?>